﻿// Revised BSD License text at bottom
using UnityEngine;
using GP.Utils;

/// <summary>
/// Tests color comparision operations.
/// </summary>
public class Test_Colors : MonoBehaviour, ITestController
{
    public static float comparisonDifference = 0.001f;
    public static float comparisonDifferenceEachValue = 0.0001f;
    public static int numIterations = 10000;
    public static Color colorTestStatic = new Color(123f/255f, 123f/ 255f, 123f/ 255f, 123f/ 255f);

    // Test floating point errors
    public static Color colorTestStaticAlmost = new Color(123.01f / 255f, 123.01f / 255f, 123.01f / 255f, 123.01f / 255f);
    public static Color32 color32TestStatic = new Color32(123, 123, 123, 123);

    // Separate instances so it's not comparing memory pointers directly
    public static Color colorUnityFloat = new Color(123f / 255f, 123f / 255f, 123f / 255f, 123f / 255f);
    public static Color32 color32Test = new Color32(123, 123, 123, 123);

    public void Init()
    {
    }

    public void Test()
    {
        // Reset
        Init();

        // This is here so the code actually does something.
        int matches = 0;

        TestController.profilerHelper.BeginSample("Colors : Unity Color comparison");
        {
            for ( int i = numIterations -1; i > -1; --i)
            {
                if (colorUnityFloat == colorTestStatic)
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : Unity Color comparison");


        ////////////////////
        matches = 0;
        TestController.profilerHelper.BeginSample("Colors : Color_Looks_Like comparision");
        {
            for (int i = numIterations - 1; i > -1; --i)
            {
                if (Utils_Perf.Color_Looks_Like(colorUnityFloat, colorTestStaticAlmost))
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : Color_Looks_Like comparison");

        ////////////////////
        matches = 0;
        TestController.profilerHelper.BeginSample("Colors : Color32.Equals comparison");
        {
            for (int i = numIterations - 1; i > -1; --i)
            {
                if (color32Test.Equals(color32TestStatic))
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : Color32.Equals comparison");

        ////////////////////
        matches = 0;
        TestController.profilerHelper.BeginSample("Colors : Color32_Equals comparison");
        {
            for (int i = numIterations - 1; i > -1; --i)
            {
                if (Utils_Perf.Color32_Equals(color32Test,color32TestStatic))
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : Color32_Equals comparison");

        /*
        // Too slow
        matches = 0;
        TestController.profilerHelper.BeginSample("Colors : My Color rounding comparision");
        {
            for (int i = numIterations - 1; i > -1; --i)
            {
                if (Color_Equals_Round(colorUnityFloat, colorTestStaticAlmost))
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : My Color rounding comparison");
        */

        /*
        // Not much faster than Color==Color
        ////////////////////
        matches = 0;
        TestController.profilerHelper.BeginSample("Colors : Color_Equals_Distance comparision");
        {
            for (int i = numIterations - 1; i > -1; --i)
            {
                if (Color_Equals_Distance(colorUnityFloat, colorTestStaticAlmost))
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : Color_Equals_Distance comparison");
        */

        /*
        // Not much faster than Color==Color
        ////////////////////
        matches = 0;
        TestController.profilerHelper.BeginSample("Colors : Color_Equals_Each_Value comparision");
        {
            for (int i = numIterations - 1; i > -1; --i)
            {
                if (Color_Equals_Each_Value(colorUnityFloat, colorTestStaticAlmost))
                    ++matches;
            }
        }
        TestController.profilerHelper.EndSample();
        if (matches != numIterations)
            Debug.LogError("Bad code : Color_Equals_Each_Value comparison");
            */

    }

    // Matches values withing floating point error amounts
    //
    // About 10% faster than Color==Color so I wouldn't bother with it.
    //
    public static bool Color_Equals_Each_Value(Color color1, Color color2)
    {
        if ( Mathf.Abs(color1.r - color2.r) < comparisonDifferenceEachValue &&
             Mathf.Abs(color1.g - color2.g) < comparisonDifferenceEachValue &&
             Mathf.Abs(color1.b - color2.b) < comparisonDifferenceEachValue &&
             Mathf.Abs(color1.a - color2.a) < comparisonDifferenceEachValue)
        {
            return true;
        }
        return false;
    }

    // A simplified distance-style comparison
    // Reasoning : Color is different than a positional Vector4 in that
    // being different by 1 in each direction is less than
    // being different by 4 in one direction. So I compare each value
    // individually.
    //
    // About 10% faster than Color==Color so I wouldn't bother with it.
    //
    public static bool Color_Equals_Distance(Color color1, Color color2)
    {
        float colorsAdd =
            Mathf.Abs((color1.r - color2.r)) +
            Mathf.Abs((color1.g - color2.g)) +
            Mathf.Abs((color1.b - color2.b)) +
            Mathf.Abs((color1.a - color2.a));

        if (colorsAdd < comparisonDifference)
        {
            return true;
        }
        return false;
    }

    // Compare using rounding.
    //
    // Slower than Color==Color so don't bother with it.
    //
    public static bool Color_Equals_Round(Color color1, Color color2)
    {
        if ( Mathf.RoundToInt(color1.r) == Mathf.RoundToInt(color2.r) &&
              Mathf.RoundToInt(color1.g) == Mathf.RoundToInt(color2.g) &&
              Mathf.RoundToInt(color1.b) == Mathf.RoundToInt(color2.b) &&
              Mathf.RoundToInt(color1.a) == Mathf.RoundToInt(color2.a))
        {
            return true;
        }
        return false;
    }

    /*
    public void Start()
    {
        TestColorLooksLike();
    }

    private void TestColorLooksLike()
    {
        Color colorTest1 = Color.black;
        Color colorTest2 = Color.black;
        for (float r = 0f; r < 255f; ++r)
        {
            colorTest1 = new Color(r / 255f, 0, 0, 0);
            colorTest2 = new Color((r + 1f) / 255f, 0, 0, 0);

            if (Utils_Perf.Color_Looks_Like(colorTest1, colorTest2))
                Debug.LogError("Bad code : Color_Looks_Like comparison NOT : " + r + " " + colorTest1 + " " + colorTest2 +
                    " " + (byte)(colorTest1.r * 255f) + " " + (byte)(colorTest2.r * 255f));

            // These should match since they are similar byte values
            colorTest2 = new Color((r + .4f) / 255f, 0, 0, 0);

            if (!Utils_Perf.Color_Looks_Like(colorTest1, colorTest2))
                Debug.LogError("Bad code : Color_Looks_Like comparison Equal : " + r + " " + colorTest1 + " " + colorTest2);
        }
    }
    */
}

/*
Revised BSD License

Copyright(c) 2020, Garret Polk
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Garret Polk nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL GARRET POLK BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

