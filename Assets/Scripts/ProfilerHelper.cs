﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Profiling;
using System.Timers;
using System.Diagnostics;

public class ProfilerHelper
{
    public string results;
    public Stopwatch stopwatch = new Stopwatch();
    public Dictionary<string, SampleSum> dictSamples = new Dictionary<string, SampleSum>();
    public SampleSum currentSample;
    public string currentSampleName;

    public class SampleSum
    {
        public TimeSpan ts;
        public uint samples;
    }

	public void BeginSample(string sample)
    {
        if ( !dictSamples.ContainsKey (sample))
        {
            currentSample = new SampleSum();
        }
        else
            currentSample = dictSamples[sample];

        currentSampleName = sample;

        //results += sample;
        stopwatch.Reset();
        stopwatch.Start();

        // Start sampling last
        Profiler.BeginSample(sample);
    }

    public void EndSample()
    {
        // End sampling first
        Profiler.EndSample();

        TimeSpan ts = stopwatch.Elapsed;

        currentSample.ts += ts;
        currentSample.samples++;
        stopwatch.Stop();

        dictSamples[currentSampleName] = currentSample;
    }

    /// <summary>
    /// CSV output or easily readable
    /// </summary>
    /// <returns></returns>
    public string GetResults(bool tsvOutput)
    {
        string results = "";
        //long tickFreq = Stopwatch.Frequency;

        if (tsvOutput)
        {
            // Headers
            results = "Test name\titerations\ttime (ms)\tavg time (ms)" + Environment.NewLine;
        }

        foreach ( KeyValuePair<string, SampleSum> pair in dictSamples)
        {
            string sampleName = pair.Key;
            SampleSum sampleSum = pair.Value;
            TimeSpan ts = sampleSum.ts;
            if ( sampleSum.samples == 0 )
            {
                UnityEngine.Debug.LogError("0 samples : " + pair.Key);
                continue;
            }
            TimeSpan tsAvg = new TimeSpan (ts.Ticks / sampleSum.samples);

            double ns = 1000000000.0 * (double)ts.Ticks / Stopwatch.Frequency;
            double ms = ns / 1000000.0;

            double nsAvg = 1000000000.0 * (double)ts.Ticks / Stopwatch.Frequency;
            double msAvg = ns / 1000000.0 / (double) sampleSum.samples;

            if (tsvOutput)
            {
                // Format results
                results += String.Format("{0}\t{1}\t{2}\t{3}{4}",
                    sampleName,
                    sampleSum.samples,
                    ms,
                    msAvg,
                    Environment.NewLine);
            }
            else
            {
                results += String.Format("{0} {1} T(ms):{2} A(ms):{3}{4}",
                    sampleName,
                    sampleSum.samples,
                    ms,
                    msAvg,
                    Environment.NewLine);
            }
        }

        return results;
    }
}